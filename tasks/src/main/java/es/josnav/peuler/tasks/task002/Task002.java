package es.josnav.peuler.tasks.task002;

public class Task002 {
  private static final int MAX_FIBONACCI_VALUE = 4000000;

  // https://projecteuler.net/problem=2
  public static void main(String[] args) {
    System.out.println(calculateEvenFibonacciSum(MAX_FIBONACCI_VALUE));
  }

  static int calculateNext(int previous, int current) {
    return previous + current;
  }

  static boolean isEven(int number) {
    return number % 2 == 0;
  }

  static int calculateEvenFibonacciSum(int maxFibonacciValue) {
    int previous = 1;
    int current = 2;
    int evenNumberSum = 0;

    if (maxFibonacciValue == 1) {
      return 0;
    }

    while (current <= maxFibonacciValue) {

      if (isEven(current)) {
        evenNumberSum += current;
      }

      int next = calculateNext(previous, current);

      previous = current;
      current = next;


    }

    return evenNumberSum;
  }
}
