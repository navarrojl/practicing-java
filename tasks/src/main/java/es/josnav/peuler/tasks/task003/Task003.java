package es.josnav.peuler.tasks.task003;

import es.josnav.peuler.tools.operations.Operations;

import java.util.LinkedList;

public class Task003 {
  /**
   * Main method for Task03.
   */
  public static void main(String[] args) {
    long compositeNumber = 600851475143L;

    System.out.println(calculatePrimeFactors(compositeNumber));
  }

  static LinkedList<Long> calculatePrimeFactors(long compositeNumber)
          throws UnsupportedOperationException {
    LinkedList<Long> primeFactors = new LinkedList<>();
    Operations op = new Operations();

    // Since Pollard's Rho have a small chance of failing, we should have some
    // tolerance and try again few times.
    int attempts = 5;

    // We are done when the current composite number is 1.
    long currentCompositeNumber = compositeNumber;
    while (currentCompositeNumber != 1) {
      try {
        primeFactors.add(op.primeFactorsPollardRho(currentCompositeNumber, compositeNumber));

        currentCompositeNumber = currentCompositeNumber / primeFactors.getLast();
      } catch (UnsupportedOperationException e) {
        attempts -= 1;

        if (attempts == 0) {
          throw e;
        }
      }
    }

    return primeFactors;
  }
}
