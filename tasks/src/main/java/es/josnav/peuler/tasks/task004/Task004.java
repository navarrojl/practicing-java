package es.josnav.peuler.tasks.task004;

public class Task004 {
  /**
   * Main method for Task004.
   */
  public static void main(String[] args) {
    int firstElement = 999;
    int secondElement = 999;
    int biggestPalindrome = 0;

    for (int i = 999; i > 0; i--) {
      for (int p = 999; p > 0; p--) {
        int palindromeCandidate = i * p;

        if (isPalindrome(palindromeCandidate) && palindromeCandidate > biggestPalindrome) {
          biggestPalindrome = palindromeCandidate;
          firstElement = i;
          secondElement = p;
        }
      }
    }


    System.out.println("The biggest palindrome is " + biggestPalindrome + " First:" + firstElement
            + " Second:" + secondElement);

  }

  /**
   * Tells if a number is or not palindrome using brute force.
   *
   * @param number The number we want to check if it is palindrome.
   * @return If the number is a palindrome, returns True.
   */
  static boolean isPalindrome(int number) {
    String numberAsString = Integer.toString(number);
    int numberLength = numberAsString.length();
    // For odd numbers, we ignore the middle number by doing this.
    int middlePoint = numberAsString.length() / 2;
    boolean isPalindrome = true;

    for (int i = 0; i <= middlePoint; i++) {
      if (numberAsString.charAt(i) != numberAsString.charAt(numberLength - 1 - i)) {
        isPalindrome = false;
        break;
      }
    }

    return isPalindrome;
  }

}
