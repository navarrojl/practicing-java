package es.josnav.peuler.tasks.task001;

public class Task001 {
  private static final int TOPLIMIT = 1000;

  // https://projecteuler.net/problem=1
  public static void main(String[] args) {

    System.out.println(calculate(TOPLIMIT));
  }

  static int calculate(int limit) {
    int accumulator = 0;

    for (int i = 0; i < limit; i++) {
      if (i % 3 == 0 || i % 5 == 0) {
        accumulator = accumulator + i;
      }
    }

    return accumulator;
  }
}
