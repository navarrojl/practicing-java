package es.josnav.peuler.tasks.task008;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Task008Test {
    @Test
    void testParseSingleDigitFromString() {
        assertEquals(5, Task008.getNumberFromString("12345", 4));
        assertEquals(1, Task008.getNumberFromString("12345", 0));
        assertEquals(3, Task008.getNumberFromString("12345", 2));
    }

    @Test
    void test() {
        assertEquals(7 * 8 * 9, Task008.getBiggestProduct("123456789", 3));
    }
}
