package es.josnav.peuler.tasks.task001;


import org.junit.jupiter.api.Test;

import static es.josnav.peuler.tasks.task001.Task001.calculate;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Task001Test {
    @Test
    void sumAllThreeOrFiveMultipleLesserThanTen() {
        assertEquals(23, calculate(10));
    }
}
