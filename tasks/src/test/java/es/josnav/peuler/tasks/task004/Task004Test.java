package es.josnav.peuler.tasks.task004;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Task004Test {

    @Test
    void checkIfANumberIsPalindrome() {
        assertTrue(Task004.isPalindrome(2002));
        assertTrue(Task004.isPalindrome(9999));
        assertTrue(Task004.isPalindrome(202));
        assertTrue(Task004.isPalindrome(999));
        assertTrue(Task004.isPalindrome(90909));
        assertTrue(Task004.isPalindrome(99999));
        assertTrue(Task004.isPalindrome(9009));
        assertTrue(Task004.isPalindrome(909909));
        assertTrue(Task004.isPalindrome(990099));
    }

    @Test
    void checkNumbersThatAreNotPalindrome() {
        assertFalse(Task004.isPalindrome(1234));
        assertFalse(Task004.isPalindrome(4321));
        assertFalse(Task004.isPalindrome(123));
        assertFalse(Task004.isPalindrome(321));
    }
}
