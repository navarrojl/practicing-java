package es.josnav.peuler.tasks.task003;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static es.josnav.peuler.tasks.task003.Task003.calculatePrimeFactors;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Task003Test {
    @Test
    void allPrimeFactorsAreCalculated() {
        Set<Long> primeFactorsFor13195 = new HashSet<>(Arrays.asList(5L, 7L, 13L, 29L));
        assertEquals(primeFactorsFor13195, new HashSet<>(calculatePrimeFactors(13195)));
    }
}
