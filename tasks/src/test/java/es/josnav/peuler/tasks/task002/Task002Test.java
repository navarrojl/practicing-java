package es.josnav.peuler.tasks.task002;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Task002Test {
    @Test
    void calculateNextFibonacciNumber() {
        assertEquals(3, Task002.calculateNext(1, 2));
        assertEquals(5, Task002.calculateNext(2, 3));
        assertEquals(8, Task002.calculateNext(3, 5));
    }

    @Test
    void checkIfANumberIsEven() {
        assertFalse(Task002.isEven(1));
        assertTrue(Task002.isEven(2));
        assertFalse(Task002.isEven(3));
        assertTrue(Task002.isEven(4));
    }

    @Test
    void calculateSumOfFirstEvenFibonacciNumbers() {
        assertEquals(44, Task002.calculateEvenFibonacciSum(89));
    }
}
