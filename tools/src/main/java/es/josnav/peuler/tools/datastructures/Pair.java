package es.josnav.peuler.tools.datastructures;

import lombok.Data;

@Data
class Pair<K, V> {
  private final K key;
  private V value;
}
