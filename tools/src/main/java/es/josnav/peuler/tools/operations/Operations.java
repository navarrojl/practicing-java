package es.josnav.peuler.tools.operations;

import java.math.BigInteger;

public class Operations {
  int factorial(int number) {
    int result = 1;
    for (int i = number; i > 0; i--) {
      result = result * i;
    }
    return result;
  }

  long factorial(long number) {
    long result = 1;
    for (long i = number; i > 0; i--) {
      result = result * i;
    }
    return result;
  }

  // REF: https://en.wikipedia.org/wiki/Wilson%27s_theorem
  boolean wilsonPrime(int number) {
    if (number == 1) {
      return false;
    } else {
      int previous = number - 1;
      int previousFactorial = factorial(previous);
      return (previous == (previousFactorial % number));
    }
  }

  // REF: https://en.wikipedia.org/wiki/Wilson%27s_theorem
  boolean wilsonPrime(long number) {
    if (number == 1) {
      return false;
    } else {
      long previous = number - 1;
      long previousFactorial = factorial(previous);
      return (previous == (previousFactorial % number));
    }
  }

  /**
   Calculates the Greater Common Divisor (GCD) of two given integers.
   @param firstNumber               First element.
    * @param secondNumber              Second element.
   * @return                          The greater common divisor between First and Second element.
   */
  int greaterCommonDivisor(int firstNumber, int secondNumber) {
    BigInteger firstBigNumber = BigInteger.valueOf(firstNumber);
    BigInteger secondBigNumber = BigInteger.valueOf(secondNumber);

    return firstBigNumber.gcd(secondBigNumber).intValue();
  }

  /**
   * Calculates the Greater Common Divisor (GCD) of two given long.
   * @param firstNumber               First element.
   * @param secondNumber              Second element.
   * @return                          The greater common divisor between First and Second element.
   */
  long greaterCommonDivisor(long firstNumber, long secondNumber) {
    BigInteger firstBigNumber = BigInteger.valueOf(firstNumber);
    BigInteger secondBigNumber = BigInteger.valueOf(secondNumber);

    return firstBigNumber.gcd(secondBigNumber).longValue();
  }

  /**
   * Calculates prime factors using the Pollard's rho algorithm.
   * https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm
   * @param compositeNumber                   The number we want to calculate the prime factor from.
   * @return                                  A prime factor of `compositeNumber`.
   * @throws UnsupportedOperationException    Sometimes Pollard's Rho can fail to find a factor.
   */
  public long primeFactorsPollardRho(long compositeNumber, long initiallyFixedValue)
          throws UnsupportedOperationException {
    long nonFixedValue = initiallyFixedValue;
    int cycleSize = 2;

    if (compositeNumber % 2 == 0) {
      return compositeNumber;
    }

    long potentialFactor = 1;
    while (potentialFactor == 1) {
      int count = 0;
      while (count <= cycleSize && potentialFactor <= 1) {
        nonFixedValue = pollardRhoFunctionG(compositeNumber, nonFixedValue);

        potentialFactor = greaterCommonDivisor(Math.abs(nonFixedValue - initiallyFixedValue),
                compositeNumber);

        count++;
      }

      cycleSize *= 2;
      initiallyFixedValue = nonFixedValue;
    }

    if (potentialFactor == compositeNumber && wilsonPrime(potentialFactor)) {
      throw new UnsupportedOperationException("Could not calculate any factor, invoke again.");
    } else {
      return potentialFactor;
    }
  }

  /**
   * Function g(x) from Pollard's Rho algorithm.
   * g(x) = ( x^2 + 1 ) mod n
   * @param seed      The number we want to calculate the prime factor from.
   * @param factor    Seed used to generate a pseudo-random sequence.
   * @return          The next value of the pseudo-random sequence.
   */
  @org.jetbrains.annotations.Contract(pure = true)
  private long pollardRhoFunctionG(long seed, long factor) {
    return Math.abs(((factor * factor) + 1) % seed);
  }
}
