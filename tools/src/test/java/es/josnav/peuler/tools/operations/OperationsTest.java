package es.josnav.peuler.tools.operations;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {

    @Test
    void factorialIsCalculatedCorrectly() {
        Operations op = new Operations();

        assertEquals(2, op.factorial(2));
        assertEquals(6, op.factorial(3));
        assertEquals(24, op.factorial(4));
        assertEquals(120, op.factorial(5));
    }

    @Test
    void UseWilsonToCalculateIfNumberIsPrime() {
        Operations op = new Operations();

        assertFalse(op.wilsonPrime(1));
        assertFalse(op.wilsonPrime(4));
        assertTrue(op.wilsonPrime(5));
    }

    @Test
    void euclideanGreaterCommonDivisorIsCorrect() {
        Operations op = new Operations();

        assertEquals(21, op.greaterCommonDivisor(1071, 462));
        assertEquals(21, op.greaterCommonDivisor(462, 1071));
        assertEquals(1, op.greaterCommonDivisor(387, 4325));
        assertEquals(1, op.greaterCommonDivisor(4325, 387));
    }

    @Test
    void primeFactorsCalculatedWithPollardsRhoAreCorrect() {
        Operations op = new Operations();

        Long testComposite = 13195L;
        Set<Long> factorsOf13195 = new HashSet<>(Arrays.asList(5L,7L,13L,29L));
        Set<Long> calculatedFactors = new HashSet<>();



        // fail with 3
        while (testComposite > 1) {
            Long newFactor = op.primeFactorsPollardRho(testComposite, 2);
            calculatedFactors.add(newFactor);
            testComposite /= newFactor;
        }

        assertEquals(factorsOf13195,calculatedFactors);

    }
}
