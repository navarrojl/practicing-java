package es.josnav.peuler.tools.datastructures;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PairTest {
    @Test
    void pairCanBeCreatedAndHoldIntegerValues() {
        Pair<Integer, Integer> myTestPair = new Pair<Integer, Integer>(0);
        myTestPair.setValue(5);
        assertEquals(5, myTestPair.getValue().intValue());
        assertEquals(0, myTestPair.getKey().intValue());
    }
}
